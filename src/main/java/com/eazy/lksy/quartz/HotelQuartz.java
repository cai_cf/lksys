package com.eazy.lksy.quartz;

import org.apache.log4j.Logger;
import org.quartz.DisallowConcurrentExecution;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

/**
 * 定时更新酒店数据
 * @author jzx
 * @date 2016/2/1
 * http://blog.csdn.net/evankaka/article/details/45540885
 * http://blog.csdn.net/evankaka/article/details/45556207
 */
@DisallowConcurrentExecution
public class HotelQuartz implements Job {

	private static final Logger logger = Logger.getLogger(HotelQuartz.class);    
	
	@Override
	public void execute(JobExecutionContext context) throws JobExecutionException {
		logger.debug("开始定时 ---- 抓取数据");
		//CatchHotelInfo.
	}

}
