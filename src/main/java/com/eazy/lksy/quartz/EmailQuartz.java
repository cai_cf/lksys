package com.eazy.lksy.quartz;


import org.apache.log4j.Logger;
import org.quartz.DisallowConcurrentExecution;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import com.eazy.lksy.email.MailConfig;

/**
 * http://www.iteye.com/topic/399980
 * http://snailxr.iteye.com/blog/2076903 
 */
@DisallowConcurrentExecution
public class EmailQuartz implements Job {

	private static final Logger logger = Logger.getLogger(EmailQuartz.class);    
	
	@Override
	public void execute(JobExecutionContext context) throws JobExecutionException {
		logger.debug("email 邮件发送....");
		MailConfig.getInstance().sendHotelInfo();
	}
}
