package com.eazy.lksy.utils;

import java.io.IOException;
import java.lang.reflect.Field;
import java.net.MalformedURLException;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.xml.sax.SAXException;

import com.meterware.httpunit.GetMethodWebRequest;
import com.meterware.httpunit.HttpUnitOptions;
import com.meterware.httpunit.PostMethodWebRequest;
import com.meterware.httpunit.WebConversation;
import com.meterware.httpunit.WebForm;
import com.meterware.httpunit.WebLink;
import com.meterware.httpunit.WebRequest;
import com.meterware.httpunit.WebResponse;

/**
 * @author lksy
 * @date 2016/2/15
 */
public class Unit {

	private static Log log = LogFactory.getLog(Unit.class);

	public static String getHtmlContent(String url) {
		log.info("开始发消息！");
		try {
			HttpUnitOptions.setScriptingEnabled(false);
			// 建立一个WebConversation实例
			WebConversation wc = new WebConversation();
			// 向指定的URL发出请求，获取响应
			WebResponse wr = wc.getResponse(url);
			// 用getText方法获取相应的全部内容
			String result = wr.getText();
			result = StrKit.strSubstringBetween(result, result.indexOf('(') + 1, result.length() - 1);
			return result;
		} catch (IOException e) {
			e.printStackTrace();
		} catch (SAXException e) {
			e.printStackTrace();
		}
		return null;
	}

	public static String getRequestMethod(String url, Map<String, String> map)
			throws MalformedURLException, IOException, SAXException {
		log.info("使用Get方式向服务器发送数据，然后获取网页内容：");
		// 建立一个WebConversation实例
		WebConversation wc = new WebConversation();
		// 向指定的URL发出请求
		WebRequest req = new GetMethodWebRequest(url);
		// 给请求加上参数
		if (map != null) {
			for (Map.Entry<String, String> m : map.entrySet()) {
				req.setParameter(m.getKey(), m.getValue());
			}
		}
		// 获取响应对象
		WebResponse resp = wc.getResponse(req);
		// 用getText方法获取相应的全部内容
		String result = resp.getText();
		return result;
	}

	public static String getRequestMethod(String url) throws MalformedURLException, IOException, SAXException {
		return getRequestMethod(url, null);
	}

	public static String postRequestClassParamMethod(String url, Map<String, Object> map)
			throws MalformedURLException, IOException, SAXException, IllegalArgumentException, IllegalAccessException {
		log.info("使用Post方式向服务器发送数据，然后获取网页内容：");
		// 建立一个WebConversation实例
		WebConversation wc = new WebConversation();
		// 向指定的URL发出请求
		WebRequest req = new PostMethodWebRequest(url);
		// 给请求加上参数
		for (Map.Entry<String, Object> m : map.entrySet()) {
			Object cz = m.getValue();
			Field[] field = cz.getClass().getDeclaredFields();
			// 直接忽略 serialVersionUID
			for (int i = 1; i < field.length; i++) {
				field[i].setAccessible(true);
				req.setParameter(field[i].getName(), field[i].get(cz).toString());
			}
		}
		// 获取响应对象
		WebResponse resp = wc.getResponse(req);
		// 用getText方法获取相应的全部内容
		String result = resp.getText();
		return result;
	}

	public static String postRequestStrParamMethod(String url, Map<String, String> map)
			throws MalformedURLException, IOException, SAXException {
		log.info("使用Post方式向服务器发送数据，然后获取网页内容：");
		// 建立一个WebConversation实例
		WebConversation wc = new WebConversation();
		// 向指定的URL发出请求
		WebRequest req = new PostMethodWebRequest(url);
		// 给请求加上参数
		for (Entry<String, String> m : map.entrySet()) {
			req.setParameter(m.getKey(), m.getValue());
		}
		// 获取响应对象
		WebResponse resp = wc.getResponse(req);
		// 用getText方法获取相应的全部内容
		String result = resp.getText();
		return result;
	}

	public static String clickLink(String url, String click) throws MalformedURLException, IOException, SAXException {
		HttpUnitOptions.setScriptingEnabled(false);
		// 代替浏览器作用的对象
		WebConversation webConversation = new WebConversation();
		// 要测试的页面的请求
		WebRequest webRequest = new PostMethodWebRequest(url);
		WebResponse webResponse = webConversation.getResponse(webRequest);
		// 获取文本为“testlink”的超链接
		System.out.println(webResponse.getText());
		WebLink webLink = webResponse.getLinkWith(click);
		// 获取超链接对应的请求
		WebRequest linkRequest = webLink.getRequest();
		// 获取超链接点击后对应的响应
		WebResponse linkResponse = webConversation.getResponse(linkRequest);
		return linkResponse.getText();
	}

	/**
	 * 测试WebForm的处理表单和提交能力
	 */
	public static void testFormSubmit() {
		HttpUnitOptions.setScriptingEnabled(false);
		WebConversation wc = new WebConversation();
		WebRequest request = new PostMethodWebRequest("http://baidu.com/li/");
		WebResponse response = null;
		try {
			response = wc.getResponse(request);
			// 获得Html中的form表单，HttpUnit将他封装成WebForm对象
			WebForm form = response.getForms()[0];
			// WebForm对象提供getParameterValue的方法将根据表单中的name获得对应的value,而不用管该元素的类型。
			// 对表单进行处理操作
			form.setParameter("username", "le");
			form.setParameter("password", "le");
			// 提交表单 获得新的response
			response = form.submit();
			System.out.println(response.getText());
			System.out.println("----------------------------");
			// 获得页面链接对象
			WebLink link = response.getLinkWith("新闻中心");
			// 模拟用户单击事件
			link.click();
			// 获得当前的响应对象
			WebResponse nextLink = wc.getCurrentPage();
			// 用getText方法获取相应的全部内容
			// 用System.out.println将获取的内容打印在控制台上
			System.out.println(nextLink.getText());
		} catch (IOException e) {
			e.printStackTrace();
		} catch (SAXException e) {
			e.printStackTrace();
		}
	}
}
