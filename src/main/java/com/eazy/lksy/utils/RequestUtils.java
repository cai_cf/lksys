package com.eazy.lksy.utils;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


/**
 * @author jzx
 * http 请求处理工具类
 * @date 2016/2/6
 */
public class RequestUtils {

	public static Map<String,String> actionRequest(HttpServletRequest request) {
		try {
			Map<String,String> hashMap = new HashMap<String,String>();
			@SuppressWarnings("unchecked")
			Map<String,String [] > map = request.getParameterMap();  
			Set<String> keySet = map.keySet();
			for (String key : keySet) {
				String[] values = map.get(key);  
				for (String value : values) {  
					hashMap.put(key, new String(value.getBytes("ISO-8859-1"),"utf-8"));
			    }  
			}
			return hashMap;
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public static void setAttr(HttpServletRequest request,String key,Object value) {
		request.setAttribute(key, value);
	}
	
	public static void setAttrs(HttpServletRequest request,Map<String,Object> map) {
		for(Map.Entry<String, Object> m : map.entrySet()) {
			request.setAttribute(m.getKey(), m.getValue());
		}
	}
	
	public static void resultJson(HttpServletResponse response) {
	}
	
	
}
