package com.eazy.lksy.utils;

import java.util.List;
import java.util.Map;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;

/**
 * @date 2015/1/30
 * @author apple
 */
public class JsonKsy {
	
	static Map<String,JSONObject> map = Maps.newHashMap();
	
	public static void put(String key,Object value) {
		JSONObject jsonObject = new JSONObject();
		jsonObject.put(key, value);
		map.put(key, jsonObject);
	}
	
	public static String parseJSON(String json) {
		return JSONObject.toJSONString(json);
	}
	
	public static String converMapToJson(Object map) {
		return JSON.toJSONString(map);
	}
	
	public static String get(String key) {
		JSONObject js = map.get(key);
		return js.getString(key);
	}
	
	public static String parseObj(String data,String param) {
		JSONObject jsonObject = JSONObject.parseObject(data);
		String result = jsonObject.getString(param); // 处理结果
		return result;
	}
	
	public static String [] parseArr(String data,String param) {
		JSONArray jsonArray = JSONArray.parseArray(data);
		List<String> lists = Lists.newArrayList();
		for(int i=0; i< jsonArray.size(); i++) {
			JSONObject jsonObject = jsonArray.getJSONObject(i);
			lists.add(jsonObject.getString(param));
		}
		return StrKit.converStr(lists);
	}
	
	public static String [] parseArr(String data,String ... param) {
		JSONArray jsonArray = JSONArray.parseArray(data);
		List<String> lists = Lists.newArrayList();
		for(int i=0; i< jsonArray.size(); i++) {
			JSONObject jsonObject = jsonArray.getJSONObject(i);
			for(int j =0; j< param.length; j++) {
				lists.add(jsonObject.getString(param[j]));	
			}
		}
		return StrKit.converStr(lists);
	}
	
	/**
	 * 2016/2/19 新加
	 */
	
}
