package com.eazy.lksy.utils;

import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class DateUtils {

	private static String yyyyMMdd = "YYYY-MM-dd";
	private static String yyyyMMddHHMMSS = "YYYY-MM-dd HH:mm:SS";
	private static DateFormat format = new SimpleDateFormat(yyyyMMdd);
	
	
	public static String getNowTime() {
		Calendar cd = Calendar.getInstance();
		return format.format(cd.getTime());
	}
	
	public static String getNowTime(String dateType) {
		Calendar cd = Calendar.getInstance();
		format = new SimpleDateFormat(dateType);
		return format.format(cd.getTime());
	}
	
	/**
	 * 指定任意日期格式化
	 * @param dataType 日期类型
	 * @param anotherDay 任意日期
	 * @return 
	 */
	public static String getNowTime(String dataType, String anotherDay) {
		try {
			format = new SimpleDateFormat(dataType);
			Date date = format.parse(anotherDay);
			return format.format(date);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	/**
	 * 获取系统时间Timestamp
	 * @return
	 */
	public static Timestamp getSysTimestamp(){
		return new Timestamp(new Date().getTime());
	}
	
	
	
	public static void main(String[] args) {	
		System.out.println(DateUtils.getNowTime(DateUtils.yyyyMMddHHMMSS));
	}
}
