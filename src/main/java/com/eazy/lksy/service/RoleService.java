package com.eazy.lksy.service;

import java.util.List;
import java.util.Map;

public interface RoleService {

	public List<Map<String, Object>> selectRole();
	
	public String addRole(Map<String,String> map);
	
	public List<Map<String, Object>> getRolePermissions(String id);
}
