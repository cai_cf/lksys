package com.eazy.lksy.service;

import java.util.List;
import java.util.Map;

public interface LineService {

	public List<Map<String, Object>> selectLineName();
	
	public List<Map<String, Object>> selectLine(String id);
}
