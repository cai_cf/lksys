package com.eazy.lksy.service;

import java.util.List;
import java.util.Map;

public interface PermissionService {

	public List<Map<String, Object>> permissionAllList();
	
	public List<Map<String,Object>> permissionList(String currentUserId);
	
	public void addMenu(Map<String,String> map);
	
	public List<Map<String, Object>> permissionMenu();
	
	public List<Map<String, Object>> permissionSubMenu(String pid);
	
}
