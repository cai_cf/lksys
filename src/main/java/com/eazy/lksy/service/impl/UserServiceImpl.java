package com.eazy.lksy.service.impl;  
  
import java.util.List;  
import java.util.Map;  
  
import org.springframework.beans.factory.annotation.Autowired;  
import org.springframework.stereotype.Service;

import com.eazy.lksy.dao.UserDao;
import com.eazy.lksy.model.User;
import com.eazy.lksy.service.UserService;  
  
  
@Service  
public class UserServiceImpl implements UserService {  
  
    @Autowired  
    private UserDao userDao;  
      
    public List<Map<String, Object>> selectUser() {  
        return userDao.selectUser();  
    }

	@Override
	public User login(User user) {
		return userDao.login(user);
	}

	@Override
	public void delete(String id) {
		userDao.delete(id);
	}

	@Override
	public void update(User user) {
		userDao.update(user);
	}

	@Override
	public void add(User user) {
		userDao.add(user);
	}  
  
}  