package com.eazy.lksy.service.impl;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.eazy.lksy.dao.AreaDao;
import com.eazy.lksy.service.AreaService;

@Service
public class AreaServiceImpl implements AreaService {

	@Autowired
	private AreaDao areaDao; 
	
	@Override
	public List<Map<String, Object>> selectCity(String city_id) {
		return areaDao.selectCity(city_id);
	}

}
