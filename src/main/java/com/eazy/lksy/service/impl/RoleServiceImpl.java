package com.eazy.lksy.service.impl;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.eazy.lksy.dao.RoleDao;
import com.eazy.lksy.service.RoleService;

@Service
public class RoleServiceImpl implements RoleService {

	@Autowired
	private RoleDao roleDao;
	
	/**
	 * 查询所有角色
	 */
	@Override
	public List<Map<String, Object>> selectRole() {
		return roleDao.selectRole();
	}

	@Override
	public String addRole(Map<String, String> map) {
		return roleDao.addRole(map);
	}

	@Override
	public List<Map<String, Object>> getRolePermissions(String id) {
		return roleDao.getRolePermissions(id);
	}
	

}
