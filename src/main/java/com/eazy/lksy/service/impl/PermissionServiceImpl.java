package com.eazy.lksy.service.impl;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.eazy.lksy.dao.PermissionDao;
import com.eazy.lksy.service.PermissionService;

@Service
public class PermissionServiceImpl implements PermissionService {

	@Autowired
	private PermissionDao permissionDao;
	
	@Override
	public List<Map<String, Object>> permissionList(String currentUserId) {
		return permissionDao.permissionList(currentUserId);
	}

	@Override
	public void addMenu(Map<String, String> map) {
		permissionDao.addMenu(map);
	}

	@Override
	public List<Map<String, Object>> permissionAllList() {
		return permissionDao.permissionAllList();
	}

	@Override
	public List<Map<String, Object>> permissionMenu() {
		return permissionDao.permissionMenu();
	}

	@Override
	public List<Map<String, Object>> permissionSubMenu(String pid) {
		return permissionDao.permissionSubMenu(pid);
	}

}
