package com.eazy.lksy.service.impl;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.eazy.lksy.dao.LineDao;
import com.eazy.lksy.service.LineService;

@Service
public class LineServiceImpl implements LineService {

	@Autowired
	private LineDao lineDao;
	
	@Override
	public List<Map<String, Object>> selectLineName() {
		return lineDao.selectLineName();
	}

	@Override
	public List<Map<String, Object>> selectLine(String id) {
		return lineDao.selectLine(id);
	}

}
