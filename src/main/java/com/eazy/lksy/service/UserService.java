package com.eazy.lksy.service;  
  
import java.util.List;  
import java.util.Map;

import com.eazy.lksy.model.User;

  
public interface UserService {  
  
    public List<Map<String,Object>> selectUser();  
    
    public User login(User user);
    
    public void delete(String id);
    
    public void update(User user);
    
    public void add(User user);
} 