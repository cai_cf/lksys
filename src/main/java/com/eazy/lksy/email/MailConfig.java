package com.eazy.lksy.email;

import java.io.Serializable;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;

import org.springframework.core.io.ClassPathResource;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.mail.javamail.MimeMessageHelper;

import com.eazy.lksy.utils.ApplicationContextUtils;
import com.eazy.lksy.utils.StrKit;

public class MailConfig {

	private static MailConfig config = new MailConfig();

	private MailConfig() {
	}

	public static MailConfig getInstance() {
		return config;
	}


	/**
	 * 统计每个星期的新增酒店个数
	 */
	public void sendHotelInfo() {
		MailBean bean = new MailBean();
		getConfig().sendText("酒店统计", "发送消息");
	}

	ConcurrentHashMap<String, Object> concurrentHashMap = new ConcurrentHashMap<String, Object>();

	private MailConfig getConfig() {
		// 系统发送人数
		List<Map<String, Object>> map = com.eazy.lksy.email.dao.MailDao.selectEmail();
		String[] toAddress = StrKit.converStr(map, "name");

		ApplicationContextUtils context = new ApplicationContextUtils("mail.xml");
		JavaMailSenderImpl mailSender = (JavaMailSenderImpl) context.getBean("mailSender");
		concurrentHashMap.put("mailSender", mailSender);
		concurrentHashMap.put("toAddress", toAddress);
		return this;
	}

	private void sendHtml() {
		//getConfig(mailBean);
	}

	/**
	 * 发送附件
	 */
	private void sendFile() {
		JavaMailSenderImpl mailSender = (JavaMailSenderImpl) concurrentHashMap.get("mailSender");
		// 使用JavaMail的MimeMessage，支持更加复杂的邮件格式和内容
		MimeMessage msg = mailSender.createMimeMessage();
		try {
			MimeMessageHelper helper = new MimeMessageHelper(msg, true);
			helper.setTo("mosaic@126.com");
			helper.setSubject("Hello Attachment");
			helper.setText("This is a mail with attachment");
			// 加载文件资源，作为附件
			ClassPathResource file = new ClassPathResource("Chrysanthemum.jpg");
			//加入附件
		    helper.addAttachment("attachment.jpg", file);
		    //发送邮件
		    mailSender.send(msg);
		} catch (MessagingException e) {
			e.printStackTrace();
		}
	}

	/**
	 * @param subject
	 *            发送主题
	 * @param text
	 *            发送内容
	 */
	private void sendText(String subject, String text) {
		JavaMailSenderImpl mailSender = (JavaMailSenderImpl) concurrentHashMap.get("mailSender");
		SimpleMailMessage mailMessage = new SimpleMailMessage();
		String[] toAddress = (String[]) concurrentHashMap.get("toAddress");
		mailMessage.setTo(toAddress);
		mailMessage.setSubject(subject);
		mailMessage.setText(text);
		mailSender.send(mailMessage);
	}

	/**
	 * 自定义邮件发送功能
	 * 
	 * @param mailBean
	 */
	private void sendText(MailBean mailBean) {
		JavaMailSenderImpl mailSender = (JavaMailSenderImpl) concurrentHashMap.get("mailSender");
		SimpleMailMessage mailMessage = new SimpleMailMessage();
		mailMessage.setTo(mailBean.getToEmails());
		mailMessage.setSubject(mailBean.getSubject());
		mailMessage.setText((String) mailBean.getData().get("data"));
		mailSender.send(mailMessage);
	}
}

class MailBean implements Serializable {

	private String from;
	private String fromName;
	private String[] toEmails;

	private String subject;

	private Map data; // 邮件数据
	private String template; // 邮件模板

	public String getFrom() {
		return from;
	}

	public void setFrom(String from) {
		this.from = from;
	}

	public String getFromName() {
		return fromName;
	}

	public void setFromName(String fromName) {
		this.fromName = fromName;
	}

	public String[] getToEmails() {
		return toEmails;
	}

	public void setToEmails(String[] toEmails) {
		this.toEmails = toEmails;
	}

	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public Map getData() {
		return data;
	}

	public void setData(Map data) {
		this.data = data;
	}

	public String getTemplate() {
		return template;
	}

	public void setTemplate(String template) {
		this.template = template;
	}

}
