package com.eazy.lksy.email.dao;

import java.util.List;
import java.util.Map;


import com.eazy.lksy.reptile.dao.ZQ;

public class MailDao {

	/**
	 * 查询所有有效的
	 * @return
	 */
	public static List<Map<String, Object>> selectEmail() {
		return ZQ.commonResult("select * from mail where 1=1 and status = 1", null);
	}
	
	/**
	 * 删除人员
	 */
	public static void delete(String id) {
		ZQ.commonDelete("delete from mail where id=?", id);
	}
	
	
	/**
	 * 修改状态- 启用 - 关闭
	 */
	public static void update(String id) {
		ZQ.commonUpdate("update mail set status=? where id", id);
	}
	
	/**
	 * 查询指定类型
	 */
	//public static Map<String,Object> selectBy

}
