package com.eazy.lksy.dao;

import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.eazy.lksy.common.BaseDao;

@Repository
public class PermissionDao extends BaseDao {

	/**
	 *  查询用户列表
	 *  用户拥有的权限
	 */
	public List<Map<String, Object>> permissionList(String currentUserId) {
		String sql =  " SELECT p.* FROM permission p "
				+ " INNER JOIN role_permission rp ON p.ID=rp.PERMISSION_ID "
				+ " INNER JOIN role r ON  r.id=rp.ROLE_ID "
				+ " INNER JOIN user_role  ur ON ur.ROLE_ID =rp.ROLE_ID "
				+ " INNER JOIN user u ON u.id = ur.USER_ID "
				+ " WHERE u.id=? ORDER BY p.sort";
		return dao.queryForList(sql,currentUserId);
	}
	
	/**
	 * 获取所有目录
	 */
	public List<Map<String, Object>> permissionMenu() {
		String sql =  " SELECT p.* FROM permission p  WHERE pid IS NULL ORDER BY p.sort ";
		return dao.queryForList(sql);
	}
	
	/**
	 * 查询子菜单
	 */
	public List<Map<String, Object>> permissionSubMenu(String pid) {
		String sql =  " SELECT p.* FROM permission p  WHERE pid=? ORDER BY p.sort ";
		return dao.queryForList(sql,pid);
	}
	
	/**
	 * 查询拥有的权限
	 */
	public List<Map<String, Object>> permissionAllList() {
		String sql =  " SELECT p.* FROM permission p "
				+ "  ORDER BY p.sort";
		return dao.queryForList(sql);
	}
	
	/**
	 * 添加权限
	 */
	public void addMenu(Map<String,String> map) {
		String sql = "insert into permission(type,name) values(?,?)";
		dao.update(sql,new Object[]{"F",map.get("name")});
	}
	
}
