package com.eazy.lksy.dao;

import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.eazy.lksy.common.BaseDao;
import com.eazy.lksy.reptile.dao.ZQ;

@Repository
public class RoleDao extends BaseDao {

	public List<Map<String, Object>> selectRole() {
		return dao.queryForList("select * from role");
	}
	
	/**
	 * 添加角色
	 */
	public String addRole(Map<String,String> map) {
		String sql = "insert into role(name,role_code,DESCRIPTION) values(?,?,?)";
		return ZQ.commonInsertAndGeneryKey(sql, map.get("name"),map.get("role_code"),map.get("desc"));
	}
	
	/**
	 * 获取角色拥有的权限ID集合
	 */
	public List<Map<String, Object>> getRolePermissions(String id) {
		String sql = "SELECT p.* FROM role_permission rp INNER JOIN permission p ON rp.`PERMISSION_ID` = p.`ID` WHERE rp.`ROLE_ID` = ?";
		return dao.queryForList(sql,id);
	}
	
}
