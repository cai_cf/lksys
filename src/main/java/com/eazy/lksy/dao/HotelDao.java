package com.eazy.lksy.dao;

import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.eazy.lksy.common.BaseDao;

@Repository
public class HotelDao extends BaseDao {

	public List<Map<String, Object>> selectHotel() {
		return dao.queryForList("select * from hotel");
	}
}
