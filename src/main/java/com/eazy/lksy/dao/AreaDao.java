package com.eazy.lksy.dao;


import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.eazy.lksy.common.BaseDao;

@Repository
public class AreaDao extends BaseDao {

	public List<Map<String, Object>> selectCity(String city_id) {
		return dao.queryForList("select * from area where city_id=?",city_id);
	}
}
