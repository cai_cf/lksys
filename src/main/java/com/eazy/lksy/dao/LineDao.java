package com.eazy.lksy.dao;

import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.eazy.lksy.common.BaseDao;

@Repository
public class LineDao extends BaseDao {

	/**
	 * 查询线路名称
	 * @return
	 */
	public List<Map<String, Object>> selectLineName() {
		return dao.queryForList("SELECT lne.`id`,c.`name`,lne.`name`  AS lname,lne.`create_time` FROM city c INNER JOIN line_name lne ON c.`id` = lne.`city_id`  WHERE c.`status` = 0");
	}
	
	/**
	 * 查询站名
	 * @return
	 */
	public List<Map<String, Object>> selectLine(String id) {
		String sql = "SELECT * FROM line WHERE line_id=?";
		return dao.queryForList(sql,id);
	}
}
