package com.eazy.lksy.dao;

import java.util.List;
import java.util.Map;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.simple.ParameterizedBeanPropertyRowMapper;
import org.springframework.stereotype.Repository;

import com.eazy.lksy.common.BaseDao;
import com.eazy.lksy.model.User;
import com.eazy.lksy.reptile.dao.ZQ;
import com.eazy.lksy.utils.MD5;

@Repository
public class UserDao extends BaseDao {

	/**
	 * 用户查询列表
	 * @return
	 */
	public List<Map<String, Object>> selectUser() {
		return dao.queryForList("select * from user");
	}

	/**
	 * 用户登录
	 * @param user
	 * @return
	 */
	@SuppressWarnings("deprecation")
	public User login(User user) {
		String sql = "select * from user where name=? and password=?";
		RowMapper<User> rm = ParameterizedBeanPropertyRowMapper.newInstance(User.class);
		return  dao.queryForObject(sql, rm, new Object[] { user.getName(), user.getPassword() });
	}
	

	/**
	 * 删除用户id
	 * @param id
	 */
	public void delete(String id) {
		ZQ.commonDelete("user", id);
	}
	
	/**
	 * 修改用户
	 * @param user
	 */
	public void update(User user) {
		ZQ.commonUpdate("update user set password=? where id=?", new Object[]{MD5.encodeString(user.getPassword()),user.getId()});
	}
	
	/**
	 * 添加用户
	 */
	public void add(User user) {
		ZQ.commonInsert("insert into user(name,password) values(?,?)", new Object[]{user.getName(),MD5.encodeString(user.getPassword())});
	}
}