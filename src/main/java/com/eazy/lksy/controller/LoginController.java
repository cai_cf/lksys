package com.eazy.lksy.controller;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.subject.Subject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.eazy.lksy.service.PermissionService;
import com.eazy.lksy.service.UserService;
import com.eazy.lksy.utils.MD5;
import com.eazy.lksy.utils.RequestUtils;
import com.eazy.lksy.utils.StrKit;
import com.eazy.lksy.utils.UserUtil;

@Controller
@RequestMapping("/sys")
public class LoginController {

	private static Log log = LogFactory.getLog(LoginController.class);
	
	@Autowired
	private UserService userService;
	@Autowired
	private PermissionService permissionService;

	/**
	 * 用户登录
	 */
	@RequestMapping(value = "/login", method = { RequestMethod.POST, RequestMethod.GET })
	public String login(HttpServletRequest request,RedirectAttributes redirectAttributes) {
		Map<String, String> map = RequestUtils.actionRequest(request);
		
		if(StrKit.isEmpty(map.get("name")) && StrKit.isEmpty(map.get("pwd")) ) {
			redirectAttributes.addAttribute("msg", "用户名和密码不能为空");
		}
		else if(StrKit.isEmpty(map.get("name"))) {
			redirectAttributes.addAttribute("msg", "用户名不能为空");
		}
		else if(StrKit.isEmpty(map.get("pwd"))) {
			redirectAttributes.addAttribute("msg", "密码不能为空");
		} else {
			if (StrKit.notEmpty(map.get("name")) && StrKit.notEmpty(map.get("pwd"))) {
				UsernamePasswordToken token = new UsernamePasswordToken(map.get("name"), MD5.encodeString(map.get("pwd")));
				token.setRememberMe(true);
				Subject currentUser = SecurityUtils.getSubject();
				currentUser.login(token);
				// 验证是否登录成功
				if (currentUser.isAuthenticated()) {
					return "redirect:index";
				} else {
					token.clear();
				}
			}
		}
		return "redirect:fail";
	}

	/**
	 * 用户登出
	 */
	@RequestMapping(value = "/logout", method = { RequestMethod.POST, RequestMethod.GET })
	public ModelAndView logout() {
		ModelAndView andView = new ModelAndView("sys/login");
		Subject subject = SecurityUtils.getSubject();
		subject.logout();
		return andView;
	}

	/**
	 * 跳转到欢迎页
	 */
	@RequestMapping(value = "/welcome", method = RequestMethod.GET)
	public ModelAndView welcome() {
		return new ModelAndView("index/welcome");
	}

	/**
	 * 跳转到首页
	 */
	@RequestMapping(value = "/index", method = RequestMethod.GET)
	public ModelAndView index() {
		String value = UserUtil.getCurrentUser().getId().toString();
		List<Map<String,Object>> result = permissionService.permissionList(value);
		return new ModelAndView("index/index","data",result);
	}

	/**
	 * 跳转到登录失败页面
	 */
	@RequestMapping(value = "/fail", method = RequestMethod.GET)
	public ModelAndView fail(HttpServletRequest request) {
		return new ModelAndView("login/login","msg",request.getParameter("msg"));
	}
	
	/**
	 * 跳转到角色管理
	 */
	@RequestMapping(value = "/role/list", method = RequestMethod.GET)
	public String role() {
		return "redirect:/role/list";
	}
	

}
