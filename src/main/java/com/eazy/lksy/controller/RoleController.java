package com.eazy.lksy.controller;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.eazy.lksy.common.BaseController;
import com.eazy.lksy.service.PermissionService;
import com.eazy.lksy.service.RoleService;
import com.eazy.lksy.utils.RequestUtils;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;

/**
 * @date 2016/1/28
 * @author jzx 
 * @desc 角色管理
 */
@Controller  
@RequestMapping("/role")  
public class RoleController extends BaseController {

	@Autowired
	private RoleService roleService;
	@Autowired
	private PermissionService permissionService;
	
	/**
     * 查询用角色列表
     */
    @RequestMapping(value = "list" , method = RequestMethod.GET)  
    public ModelAndView selectRole() {
    	ModelAndView andView = new ModelAndView("admin/role_list");
    	List<Map<String, Object>> data = roleService.selectRole();
    	andView.addObject("data", data);
    	return andView;
    }
    
	/**
	 * 角色添加页面跳转
	 */
    @RequestMapping(value = "toAddRole" , method = RequestMethod.GET)  
    public ModelAndView toAddRole() {
    	Map<String,Object> map = Maps.newHashMap();
    	List<Map<String,Object>> all = permissionService.permissionAllList();
    	map.put("all", all);
    	return new ModelAndView("admin/role_add",map);
    }
	
    /**
     * 添加角色
     */
    @RequestMapping(value = "addRole" , method = RequestMethod.GET)
    public String addRole(HttpServletRequest request) {
    	Map<String,String> map = RequestUtils.actionRequest(request);
    	roleService.addRole(map);
    	return "redirect:/role/list";
    }
    
    /**
     * 跳转到修改角色页面
     */
    @RequestMapping(value = "toUpdateRole/{id}" , method = RequestMethod.GET)
    public ModelAndView toUpdateRole(@PathVariable String id) {
    	Map<String,Object> map = Maps.newHashMap();
    	List<Map<String,Object>> all = permissionService.permissionAllList();
    	List<Map<String,Object>> me = roleService.getRolePermissions(id);
    	List<Map<String,Object>> listAdd = Lists.newArrayList();
    	for(int i =0; i< all.size(); i++) {
    		Map<String,Object> a = all.get(i);
    		
    		if(a.get("pid") == null) {
    			
    			for(int j=0; j< me.size(); j++) {
    				Map<String,Object> m =  me.get(j);
    				if(a.get("pid") == null && m.get("pid") == null && a.get("id") == m.get("id")) {
    					a.put("menu", true);
    				}
    				if(a.get("id") == m.get("pid")) {
    					Map<String,Object> mp = Maps.newHashMap();
    					mp.put("sub_id", m.get("id"));
    					mp.put("name", m.get("name"));
    					listAdd.add(mp);
    				}
    			}
    		}
    	}
    	map.put("all", all);
    	map.put("sub_menu", listAdd);
    	return new ModelAndView("admin/role_update",map);
    }
    
    /**
     * 修改角色
     */
    @RequestMapping(value = "updateRole" , method = RequestMethod.GET)
    public String updateRole(HttpServletRequest request) {
    	Map<String,String> map = RequestUtils.actionRequest(request);
    	roleService.addRole(map);
    	return "redirect:/role/list";
    }
    
    /**
     * 删除角色
     */
    @RequestMapping(value = "deleteRole" , method = RequestMethod.GET)
    public String deleteRole(HttpServletRequest request) {
    	Map<String,String> map = RequestUtils.actionRequest(request);
    	roleService.addRole(map);
    	return "redirect:/role/list";
    }
}
