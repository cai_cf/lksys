package com.eazy.lksy.controller;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.eazy.lksy.service.AreaService;

/**
 * @author jzx
 * @date 2016/2/15
 * @desc 区域管理
 */
@Controller
@RequestMapping("/area")
public class AreaController {

	@Autowired
	private AreaService areaService;
	
	/**
	 * 区域查询
	 */
	@RequestMapping(value = "list/{id}" , method = RequestMethod.GET)
	public ModelAndView selectCity(@PathVariable String id) {
		ModelAndView andView = new ModelAndView("city/area/area_list");
		List<Map<String, Object>> data = areaService.selectCity(id);
		andView.addObject("data", data);
		return andView;
	}
	
}
