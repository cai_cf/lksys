package com.eazy.lksy.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.eazy.lksy.service.CityService;
import com.eazy.lksy.service.LineService;

/**
 * @author jzx
 * @date 2016/2/18
 * @desc 线路管理
 */
@Controller
@RequestMapping(value = "line")
public class LineController {

	@Autowired
	private CityService cityService;
	@Autowired
	private LineService lineService;
	
	@RequestMapping(value = "/list", method = RequestMethod.GET)
	public ModelAndView list() {
		ModelAndView view = new ModelAndView("city/line/line_name_list");
		view.addObject("cityData", cityService.selectCity());
		view.addObject("lineData", lineService.selectLineName());
		return view;
	}
	
	@RequestMapping(value = "/detail/{id}", method = RequestMethod.GET)
	public ModelAndView detail(@PathVariable String id) {
		ModelAndView view = new ModelAndView("city/line/line_list");
		view.addObject("lineData", lineService.selectLine(id));
		return view;
	}
}
