package com.eazy.lksy.controller;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.eazy.lksy.service.CityService;
import com.eazy.lksy.utils.Bundle;
import com.eazy.lksy.utils.RequestUtils;

/**
 * @author jzx
 * @date 2016/2/15
 * @desc 城市管理
 */
@Controller
@RequestMapping("/city")
public class CityController {

	@Autowired
	private CityService cityService;
	
	/**
	 * 城市查询
	 */
	@RequestMapping(value = "list" , method = RequestMethod.GET)
	public ModelAndView selectCity() {
		ModelAndView andView = new ModelAndView("city/city_list");
		List<Map<String, Object>> data = cityService.selectCity();
		andView.addObject("data", data);
		return andView;
	}
	
	/**
	 * 跳转到城市添加页面
	 */
	@RequestMapping(value = "toAddcity" , method = RequestMethod.GET)
	public ModelAndView toAddcity() {
		return new ModelAndView("city/city_add");
	}
	
	/**
	 * 跳转到城市修改页面
	 */
	@RequestMapping(value = "toUpdatecity" , method = RequestMethod.GET)
	public ModelAndView toUpdatecity() {
		return new ModelAndView("city/city_update");
	}
	
	/**
	 * 城市添加
	 */
	@RequestMapping(value = "addcity" , method = RequestMethod.POST)
	public void addcity(HttpServletRequest request) {
		Map<String,String> map = RequestUtils.actionRequest(request);
		String name = map.get("name");
		Map<String,Object> exists = cityService.selectColumnExis(name);
		if(exists != null) {
			String status = Bundle.convStr(exists.get("status"));
			if(status.equals("1"))
				cityService.delete(exists.get("id") + "", "0");
		} else {
			cityService.addCity(map);
		}
	}
	
	/**
	 * 城市修改
	 */
	@RequestMapping(value = "updatecity" , method = RequestMethod.GET)
	public String updatecity(HttpServletRequest request) {
		Map<String,String> map = RequestUtils.actionRequest(request);
		cityService.updateCity(map);
		return "redirect:/city/list";
	}
	
	/**
	 * 城市删除
	 */
	@RequestMapping(value = "deletecity/{id}" , method = RequestMethod.GET)
	public String deletecity(@PathVariable String id) {
		Map<String,Object> cityModel = cityService.findViewById("city", id);
		if(cityModel == null) {
			return null;
		} else {
			String status = Bundle.convStr(cityModel.get("status"));
			if(status.equals("0"))
				cityService.delete(id, "1");
			else
				cityService.delete(id, "0");
		}
		return "redirect:/city/list";
	}
}
