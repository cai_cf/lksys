package com.eazy.lksy.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.eazy.lksy.model.ScheduleJob;
import com.eazy.lksy.service.impl.ScheduleJobService;

/**
 * @date 2016/2/2
 * @author jzx 
 * @desc 任务调度
 */
@Controller
@RequestMapping("/schedule")
public class ScheduleJobController {

	@Autowired
	private ScheduleJobService scheduleJobService;
	
	/** 添加
	 * @param user
	 * @param model
	 */
	@RequestMapping(value = "add", method = RequestMethod.GET)
	public void create() {
		ScheduleJob sb = new ScheduleJob();
		sb.setClassName("com.eazy.lksy.quartz.EmailQuartz");
		sb.setStatus("1");
		sb.setCronExpression("*/5 * * * * ?");
		sb.setGroup("group2");
		sb.setName("酒店");
		scheduleJobService.add(sb);
	}

}
