package com.eazy.lksy.controller;

import org.springframework.web.servlet.ModelAndView;

/**
 * @author jzx
 * @date 2016/2/9
 *  酒店管理
 */
public class HotelController {

	/**
	 * 查询酒店列表
	 */
	public ModelAndView selectHotel() {
		ModelAndView view = new ModelAndView();
		return view;
	}
	
	/**
	 * 添加酒店信息
	 */
	public void addHotel() {
		
	}
	
	/**
	 * 修改酒店信息
	 */
	public void updateHotel() {
		
	}
}
