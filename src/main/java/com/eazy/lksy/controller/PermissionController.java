package com.eazy.lksy.controller;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.eazy.lksy.model.User;
import com.eazy.lksy.service.PermissionService;
import com.eazy.lksy.utils.Bundle;
import com.eazy.lksy.utils.JsonKsy;
import com.eazy.lksy.utils.RequestUtils;
import com.eazy.lksy.utils.UserUtil;
import com.google.common.collect.Lists;

/**
 * @date 2016/2/6
 * @author jzx 
 * @desc 权限管理
 */
@Controller
@RequestMapping("/permission")
public class PermissionController {

	@Autowired
	private PermissionService permissionService;
	
	/**
	 * 获取权限列表
	 */
	@RequestMapping(value = "list" , method = { RequestMethod.POST, RequestMethod.GET })
	public ModelAndView permissionList(HttpServletRequest request,HttpServletResponse response) throws IOException {
		ModelAndView view = new ModelAndView("admin/menu_list");
		view.addObject("dataMenu", permissionService.permissionMenu());
		return view;
	}
	
	/**
	 * tree 菜单
	 *  { id:1,   pId:0, name:"一级分类", open:true},
	 *	{ id:11,  pId:1, name:"二级分类"},
	 *	{ id:111, pId:11, name:"三级分类"},
	 *	{ id:112, pId:11, name:"三级分类"},
	 *	{ id:113, pId:11, name:"三级分类"},
	 *	{ id:114, pId:11, name:"三级分类"},
	 *	{ id:115, pId:11, name:"三级分类"},
	 *	{ id:12,  pId:1, name:"二级分类 1-2"},
	 *	{ id:121, pId:12, name:"三级分类 1-2-1"},
	 *	{ id:122, pId:12, name:"三级分类 1-2-2"},
	 */
	@RequestMapping(value = "tree" , method = { RequestMethod.POST, RequestMethod.GET })
	public void tree(HttpServletResponse response) throws IOException {
		List<Map<String,Object>> menu = permissionService.permissionMenu();
		List<Map<String,Object>> coll = Lists.newArrayList();
		for(int i =0 ; i< menu.size(); i++) {
			Map<String,Object> m = menu.get(i);
			Map<String,Object> param = new HashMap<String,Object>();
			param.put("id", m.get("id"));
			param.put("pId", m.get("pid"));
			param.put("name", m.get("name"));
			param.put("open", true);
			coll.add(param);
			List<Map<String, Object>> sub = permissionService.permissionSubMenu(Bundle.convStr(m.get("id")));
			for(int j =0; j< sub.size(); j++) {
				Map<String,Object> s = sub.get(j);
				Map<String,Object> param2 = new HashMap<String,Object>();
				param2.put("id", s.get("id"));
				param2.put("pId", s.get("pid"));
				param2.put("name", s.get("name"));
				param2.put("open", true);
				coll.add(param2);
			}
		}
		response.setContentType("application/json;charset=utf-8");
		String content = JsonKsy.converMapToJson(coll);
		response.getWriter().write(content);
	}
	
	/**
	 * 获取权限列表
	 */
	@RequestMapping(value = "submenu/{pid}" , method = { RequestMethod.POST, RequestMethod.GET })
	public ModelAndView submenu(@PathVariable String pid) throws IOException {
		ModelAndView view = new ModelAndView("admin/submenu_list");
		view.addObject("dataMenu", permissionService.permissionSubMenu(pid));
		return view;
	}
	
	/**
	 * 获取菜单列表
	 */
	@RequestMapping(value = "/menu/list", method = { RequestMethod.POST, RequestMethod.GET })
	public ModelAndView list() {
		ModelAndView andView = new ModelAndView("sys/menu");
		User user = UserUtil.getCurrentUser();
		List<Map<String,Object>> result = permissionService.permissionList(user.getId().toString());
		andView.addObject("data", result);
		return andView;
	}	
	
	/**
	 * 跳转到添加菜单页面
	 */
	@RequestMapping(value = "/menu/toAdd", method = { RequestMethod.POST, RequestMethod.GET })
	public ModelAndView toAdd(HttpServletRequest request) {
		return new ModelAndView("sys/add");
	}
	
	/**
	 * 菜单添加
	 */
	@RequestMapping(value = "/menu/add", method = { RequestMethod.POST})
	public String add(HttpServletRequest request) {
		Map<String,String> actionRequest = RequestUtils.actionRequest(request);
		permissionService.addMenu(actionRequest);
		return "redirect:/permission/menu/list";
	}
}
