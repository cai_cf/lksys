package com.eazy.lksy.common;

import java.util.Map;

public interface CommonDao {
	public Map<String,Object> findViewById(String tableName,String id);
}
