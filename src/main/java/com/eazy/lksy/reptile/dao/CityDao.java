package com.eazy.lksy.reptile.dao;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;

public class CityDao {

	public static void addCity(String str) {
		try {
			Connection conn = DbConn.getConn();

			String[] array = str.split(",");

			Statement stmt = conn.createStatement();
			stmt.executeUpdate("INSERT INTO `city`(NAME) VALUES(北京); ");

			conn.close();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {

		}
	}

	public static void main(String[] args) {
		CityDao.addCity("北京,上海,广州,深圳");
	}
}
