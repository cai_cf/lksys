package com.eazy.lksy.reptile.dao;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.datasource.DriverManagerDataSource;

@Configuration
public class SpringDB {

	private @Value("#dbProperties.url") String url;
	private @Value("#dbProperties.name") String name;
	private @Value("#dbProperties.pwd") String pwd;
	
	@Bean
	public DataSource dataSource() {
		return new DriverManagerDataSource(url,name,pwd);
	}
	
}
