package com.eazy.lksy.reptile.dao;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class LineDao {

	
	// 添加地铁线路
	public static void addLine(String line,int keyGenery) {
		try {
			Connection conn = DbConn.getConn();

			String[] array = line.split(",");
			
			for(int i =0 ; i< array.length; i++) {
				Statement stmt = conn.createStatement();
				stmt.executeUpdate("insert into line(name,line_id) values('"+array[i]+"','"+keyGenery+"')");
			}
			conn.close();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
		}
	}
	
	// 添加线路名称
	public static int addLineName(String line,int key) {
		try {
			Connection conn = DbConn.getConn();

			String[] array = line.split(",");
			
			int number = 0;
			
			for(int i =0 ; i< array.length; i++) {
				Statement stmt = conn.createStatement();
				stmt.executeUpdate("insert into line_name(name,city_id) values('"+array[i]+"','"+key+"')",Statement.RETURN_GENERATED_KEYS);
				ResultSet rs = stmt.getGeneratedKeys();
				
				if(rs.next()) {
					 number = rs.getInt(1);
				}
			}
			conn.close();
			return number;
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
		}
		return 0;
	}
}
